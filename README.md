I’m a professional freelance photographer with a studio in Collingwood, Melbourne. While I specialise in commercial and corporate photography, my folio covers a range of industries. If you would like to chat further or throw a brief my way, you can contact me at +61 3 8804 1388.

Address: Level 1, 53-57 Cambridge Street, Collingwood, VIC 3066, Australia

Phone: +61 3 8804 1388

Website: https://www.freelancephotographermelbourne.com.au
